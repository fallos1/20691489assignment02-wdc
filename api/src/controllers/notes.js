const express = require("express");
const _ = require("lodash");
const models = require("../models");

const router = express.Router();

/* *** TODO: Fill in the API endpoints for notes *** */

// GET /notes
// Returns a list of all notes.

router.get("/", (req, res) => {
  models.Note.findAll({ order: [["createdAt", "DESC"]] })
    .then((notes) => res.json(notes))
    .catch((err) => res.status(500).json({ error: err.message }));
});

// POST /notes
// Creates a new note using the posted data. The notebookId
// attribute shall specify which notebook it belongs to.
// Returns the new note.

router.post("/", (req, res) => {
  models.Note.create({
    title: req.body.title,
    content: req.body.content,
    notebookId: req.body.notebookId,
  }).then((notes) => res.json(notes));
});

// GET /notes/:noteId
// Returns a single note by ID

router.get("/:noteId", (req, res) => {
  models.Note.findOne({
    where: {
      id: req.params.noteId,
    },
  })
    .then((notes) => res.json(notes))
    .catch((err) => res.status(500).json({ error: err.message }));
});

// DELETE /notes/:noteId
// Deletes a single note by ID. Returns an empty object, {}.

router.delete("/:noteId", (req, res) => {
  models.Note.destroy({
    where: {
      id: req.params.noteId,
    },
  })
    .then((notes) => res.json({}))
    .catch((err) => res.status(500).json({ error: err.message }));
});

// PUT /notes/:noteId
// Updates the attributes of a particular note. Returns the updated note.

router.put("/:noteId", (req, res) => {
  models.Note.update(
    {
      title: req.body.title,
      content: req.body.content,
      notebookId: req.body.notebookId,
    },
    { where: { id: req.params.noteId } }
  )
    .then(() =>
      models.Note.findOne({
        where: {
          id: req.params.noteId,
        },
      })
    )
    .then((notes) => res.json(notes));
});

module.exports = router;
