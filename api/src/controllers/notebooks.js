const { request } = require("express");
const express = require("express");
const _ = require("lodash");
const models = require("../models");

const router = express.Router();

// Index
router.get("/", (req, res) => {
  models.Notebook.findAll({ order: [["createdAt", "DESC"]] })
    .then((notebooks) => res.json(notebooks))
    .catch((err) => res.status(500).json({ error: err.message }));
});

/* *** TODO: Fill in the API endpoints for notebooks *** */

// GET /notebooks/:notebookId/notes
// Returns a list of all notes for a particular notebook.

router.get("/:notebookId/notes", (req, res) => {
  models.Note.findAll({
    where: {
      notebookId: req.params.notebookId,
    },
  })
    .then((notebooks) => res.json(notebooks))
    .catch((err) => res.status(500).json({ error: err.message }));
});

// POST /notebooks
// Creates a new notebook using the posted data. Returns the new notebook.

router.post("/", (req, res) => {
  models.Notebook.create({
    title: req.body.title,
  }).then((notebooks) => res.json(notebooks));
});

// GET /notebooks/:notebookId
// Returns a single notebook by ID.

router.get("/:notebookId", (req, res) => {
  models.Notebook.findOne({
    where: {
      id: req.params.notebookId,
    },
  })
    .then((notebooks) => res.json(notebooks))
    .catch((err) => res.status(500).json({ error: err.message }));
});

// DELETE /notebooks/:notebookId
// Deletes a single notebook by ID.
// All of the notebook’s notes shall be deleted also. Returns an empty object, {}.

router.delete("/:notebookId", (req, res) => {
  models.Notebook.destroy({
    where: {
      id: req.params.notebookId,
    },
  });
  models.Note.destroy({
    where: {
      notebookId: req.params.notebookId,
    },
  })
    //.then((notebooks) => res.json(notebooks))
    .then(() => res.json({}))
    .catch((err) => res.status(500).json({ error: err.message }));
});

// PUT /notebooks/:notebookId
// Updates the attributes of a particular notebook.
// Returns the updated notebook.

router.put("/:notebookId", (req, res) => {
  models.Notebook.update(
    { title: req.body.title },
    { where: { id: req.params.notebookId } }
  )
    // .then(notebooks => res.json(notebooks))
    //.then(notebooks => console.log(notebooks))
    // .catch(err => res.status(500).json({ error: err.message }));

    .then(() =>
      models.Notebook.findOne({
        where: {
          id: req.params.notebookId,
        },
      })
    )
    .then((notebooks) => res.json(notebooks));
});

module.exports = router;
